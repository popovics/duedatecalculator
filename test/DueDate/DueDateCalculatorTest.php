<?php
new \Config\Config();

class DueDateCalculatorTest extends \PHPUnit_Framework_TestCase {

  public function testCaseInstance() {
    $testddCalclulator = new \DueDate\DueDateCalculator(strtotime("2017-01-28 12:11:00"), 3600*8*5 + 60*60*7);
    $this->assertInstanceOf("\DueDate\DueDateCalculator", $testddCalclulator);
  }
  
  public function testCaseIsInteger() {
    $testddCalclulator = new \DueDate\DueDateCalculator(strtotime("2017-01-27 12:11:00"), 3600*12);
    $this->assertInternalType("int", $testddCalclulator->getDueTime());
  }
  
  public function testCaseEqual() {
    $testddCalclulator = new \DueDate\DueDateCalculator(strtotime("2017-01-27 12:11:00"), 1);
    $this->assertEquals(strtotime("2017-01-27 12:11:01"), $testddCalclulator->getDueTime());
  }
  
  public function testCaseEqualOverHolidays() {
    $testddCalclulator = new \DueDate\DueDateCalculator(strtotime("2017-01-27 09:11:00"), 3600*12);
    $this->assertEquals(strtotime("2017-01-30 13:11:00"), $testddCalclulator->getDueTime());
  }
  
  // Test for weekend submit date
  public function testCaseWeekendSubmitDate() {
    $testddCalclulator = new \DueDate\DueDateCalculator(strtotime("2017-01-28 12:11:00"), 1);
    $this->assertEquals(true, $testddCalclulator->hasError());
    $this->assertInternalType("string", $testddCalclulator->getErrorMessage());
  }
  
  // Test for after hours submit date
  public function testCaseAfterHoursSubmitDate() {
    $testddCalclulator = new \DueDate\DueDateCalculator(strtotime("2017-01-27 08:11:00"), 1);
    $this->assertEquals(true, $testddCalclulator->hasError());
    $this->assertInternalType("string", $testddCalclulator->getErrorMessage());
  }
  
  // Test for before hours submit date
  public function testCaseBeforeHoursSubmitDate() {
    $testddCalclulator = new \DueDate\DueDateCalculator(strtotime("2017-01-27 17:11:00"), 1);
    $this->assertEquals(true, $testddCalclulator->hasError());
    $this->assertInternalType("string", $testddCalclulator->getErrorMessage());
  }
}