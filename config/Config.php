<?php
namespace Config;

class Config 
{
  
  public function __construct() 
  {
    $this->defineFileStructure();
  }
  
  /**
   * Define default timezone
   */
  protected function defineTimeZone() 
  {
    date_default_timezone_set("Europe/Budapest");
  }
  
  /**
   * Define file structure
   */
  protected function defineFileStructure() 
  {
    define("ROOT", dirname((dirname(__FILE__))) . "/");
    define("CONFIG_PATH", ROOT . "config/");
    define("LIB_PATH", ROOT . "lib/");
  }

}