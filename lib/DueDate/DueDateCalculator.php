<?php

namespace DueDate;

/**
 * DueDateCalculator calulates due time based on submit date and turnaround time
 *
 * @author    mate.popovics@gmail.com
 */
class DueDateCalculator 
{

  const MSG_WRONG_SUBMIT_DAY = "Given submit time is on holiday or submit time is after or before normal working hours, it is not allowed!";

  private $submitTime; //timestamp
  private $turnaroundTime; //seconds
  private $dueTime;
  private $workStart = "09:00:00";
  private $workEnd = "17:00:00";
  private $workdaySeconds;
  private $errorMessage;
  private $hasError = false;

  /**
   * 
   * @param integer   $submitTime
   * @param integer   $turnaroundTime
   * @return void  
   */
  public function __construct($submitTime, $turnaroundTime) 
  {
    $this->configBaseData();
    $this->submitTime = $submitTime;
    $this->turnaroundTime = $turnaroundTime;
    $this->calculateDueDate();
  }

  /**
   * 
   * Get Due Date in timestamp
   * 
   * @return integer
   */
  public function getDueTime() 
  {
    return $this->dueTime;
  }

  /**
   * 
   * Configure basic data
   * 
   * @return void 
   */
  private function configBaseData() 
  {
    $this->workdaySeconds = strtotime($this->workEnd) - strtotime($this->workStart);
  }

  /**
   * 
   * Get timestamp of next day starting time
   * It is protected against DST. On beginning and ending dates of DST the current day can be longer or shorter by 1 hour. 
   * Calculating the next day by adding only 24 hours to current day at 12:00AM, it will always gives back the next day. 
   * 
   * @param   integer $currentDayTime
   * @return  integer
   */
  private function getTimestampOfNextDay($currentDayTime) 
  {
    return strtotime(date("Y-m-d ", strtotime(date("Y-m-d 12:00:00", $currentDayTime)) + 86400) . $this->workStart);
  }

  /**
   * 
   * Calculate due date take in extra days
   * Additional protection: if the submit date is on holiday, it creates error message
   * 
   * @return integer
   */
  private function calculateDueDate() 
  {
    $endTime = 0;

    $leftTimeSeconds = $this->turnaroundTime;
    $currentDayTime = $this->submitTime;
    
    if (!$this->isWorkday($currentDayTime) || $this->isAfterHours($currentDayTime) || $this->isBeforeHours($currentDayTime)) {
      $this->errorMessage = self::MSG_WRONG_SUBMIT_DAY;
      $this->hasError = true;
      return;
    }
    
    while (!$endTime) {
      if (!$this->isWorkday($currentDayTime)) {
        $currentDayTime = $this->getTimestampOfNextDay($currentDayTime);
        continue;
      }

      $currentDayEndTime = strtotime(date("Y-m-d ", $currentDayTime) . $this->workEnd);

      if ($currentDayTime + $leftTimeSeconds <= $currentDayEndTime) {
        $endTime = $currentDayTime + $leftTimeSeconds;
      } else {
        $leftTimeSeconds = $leftTimeSeconds - ($currentDayEndTime - $currentDayTime);
        $currentDayTime = $this->getTimestampOfNextDay($currentDayTime);
      }
    }
    $this->dueTime = $endTime;
  }

  /**
   * 
   * Return hasError value
   * 
   * @return boolean
   */
  public function hasError() 
  {
    return $this->hasError;
  }
  
  /**
   * Return error message
   * @return string
   */
  public function getErrorMessage() 
  {
    return $this->errorMessage;
  }
  
  /**
   * 
   * Check if work day or not based on day of given timestamp
   * 
   * @param integer $timestamp
   * @return boolean
   */
  private function isWorkday($timestamp) 
  {
    if ((date("N", $timestamp) > 5)) {
      return false;
    }
    return true;
  }
  
  /**
   * 
   * Check it the timestamp is after normal working hours
   * 
   * @param integer $timestamp
   * @return boolean
   */
  private function isAfterHours($timestamp) 
  {
    $currentDayEndTime = strtotime(date("Y-m-d ", $timestamp) . $this->workEnd);
    if ($currentDayEndTime < $timestamp) {
      return true;
    }   
    return false;
  }
  
  /**
   * 
   * Check it the timestamp is before normal working hours
   * @param integer $timestamp
   * @return boolean
   */
  private function isBeforeHours($timestamp) 
  {
    $currentDayStartTime = strtotime(date("Y-m-d ", $timestamp) . $this->workStart);
    if ($currentDayStartTime > $timestamp) {
      return true;
    }   
    return false;
  }
          
  
}