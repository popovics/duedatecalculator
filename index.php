<?php
require __DIR__ . '/vendor/autoload.php';

new \Config\Config();

$submitTime = "2017-01-27 09:11:00";
$turnaroundTime = 60 * 60 * 12;

$dueTime = new \DueDate\DueDateCalculator(strtotime($submitTime), $turnaroundTime);

if ($dueTime->hasError() == false) {
  echo "Submit date: " . $submitTime . PHP_EOL;
  echo "Turnaround time: " . gmdate("H:i:s", $turnaroundTime) . PHP_EOL;
  echo "Due date: " . date("Y-m-d H:i:s", $dueTime->getDueTime()) . PHP_EOL; 
} else {
  echo $dueTime->getErrorMessage() . PHP_EOL;
}